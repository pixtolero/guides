# Guide Post-Installation Fedora

![Cdnfure](


## Mise à jour du système

    sudo dnf update
    
## Dépôt RPM Fusion
    
    sudo rpm -Uvh http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
    sudo rpm -Uvh http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
    
## Rajouter le dépôt Flapak Flathub

    flatpak remote-add-if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

## GNOME

### Pour gérer les extensions GNOME

    sudo dnf install gnome-extensions-app
    
Ou si vous préférez Tweaks:

    sudo dnf install gnome-tweaks

  * Pour les portable, il y a l'option pour afficher le pourcentage de la batterie


## Logiciels à installer

### Codecs

Il faut le dépôt RPM Fusion ajouté (voir plus haut)

    sudo dnf groupupdate Multimedia

### Suite bureautique

    sudo dnf update
    sudo dnf install libreoffice

### Pour la Photo

    sudo dnf update
    sudo dnf install gimp gmic
    sudo dnf install darktable shotwell rapid-photo-downloader


### Jeux

    sudo dnf install steam

## Optimisation


### TLP pour la batterie

    sudo dnf install tlp tlp-rdw
    sudo systemctl enable tlp

### Changer le nom d'hôte

Par défaut c'est *localhost*, par exemple si vous voulez fedora:

    hostnamectl set-hostname fedora

### Driver NVIDIA spécfique

Il faut le dépôt RPM fusion ajouté (voir plus haut)

    modinfo -F version nvidia
    sudo dnf update -y # and reboot if you are not on the latest kernel
    sudo dnf install -y akmod-nvidia # rhel/centos users can use kmod-nvidia instead
    sudo dnf install -y xorg-x11-drv-nvidia-cuda #optional for cuda/nvdec/nvenc support
    sudo dnf install -y xorg-x11-drv-nvidia-cuda-libs
    sudo dnf install -y vdpauinfo libva-vdpau-driver libva-utils
    sudo dnf install -y vulkan
    modinfo -F version nvidia
    
    
Pour les cartes hybrides, lancer les applications avec **__NV_PRIME_RENDER_OFFLOAD=1 __VK_LAYER_NV_optimus=NVIDIA application**

Source: https://rpmfusion.org/Howto/Optimus

À compléter...
