# Guide Android No Google

## ROM + NanoDroid

Installer la rom (sans gapps) de votre choix.

Pour Nanodroid, on va [ici](https://downloads.nanolx.org/NanoDroid/Stable/).

Avec TWRP il faut installer les zip suivants

*NanoDroid*

*NanoDroid-microG*

*NanoDroid-fdroid*

*NanoDroid-BromiteWebView*

*NanoDroid-OsmAnd*

*NanoDroid-patcher*

Version alternative de microG [ici](https://github.com/friendlyneighborhoodshane/minmicrog_releases/releases)

## Magisk (accès root)

Téléchager l'apk de magiskmanager et le zip de magisk (à installer avec TWRP) [ici](https://magisk.me/).

## Signature proofing

Avec termux executer ces commandes:

    su
    pm grant com.android.vending android.permission.FAKE_PACKAGE_SIGNATURE


## Dépôts à ajouter (ou activer) aux paramètres de f-droid

*NanoDroid* [ici](https://www.nanolx.org/fdroid/repo/)

*Bromite* [ici](https://www.bromite.org/fdroid)

*MicroG* [ici](https://microg.org/fdroid.html)

*Guardian Project* [ici](https://guardianproject.info/fdroid/repo/)

*IzzyOnDroid* [ici](https://apt.izzysoft.de/fdroid/index.php)

*Firefox Unofficial* [ici](https://gitlab.com/rfc2822/fdroid-firefox)

*Bitwarden* [ici](https://mobileapp.bitwarden.com/fdroid/)

*Briar* [ici](https://briarproject.org/installing-briar-via-f-droid/)

*Ember's repo* [ici](https://fdroid.heartshine.xyz/)

*Nethunter* [ici](https://store.nethunter.com/)

*ProtonMail Unofficial* [ici](https://unofficial-protonmail-repository.gitlab.io/unofficial-protonmail-repository/fdroid/repo/)

*CalyxOS* [ici](https://calyxos.gitlab.io/calyx-fdroid-repo/fdroid/repo)

*DivestOS* [ici](https://divestos.org/fdroid/official/?fingerprint=E4BE8D6ABFA4D9D4FEEF03CDDA7FF62A73FD64B75566F6DD4E5E577550BE8467)

*FluffyChat* [ici](https://christianpauly.gitlab.io/fluffychat-website/en/fdroid.html)

*PartidoPirata* [ici](https://fdroid.partidopirata.com.ar/fdroid/repo/)

*Rakshazi* [ici](https://fdroid.rakshazi.me/)

[source](https://android.izzysoft.de/articles/named/list-of-fdroid-repos?lang=en)


## Applications indispensables

[OpenWeatherMap Provider](https://www.apkmirror.com/apk/lineageos/openweathermap/openweathermap-1-0-release/openweathermap-1-0-android-apk-download/)

À Récupérer sur les dépôts:


|Application    |Description|
|---------------|--------|
|Material Files |Navigation de fichiers|
|OpenBoard      |Clavier alternatif|
|Etar           |Calendrier, mieux que celui installé par défaut|
|NewPipe        |Voir vidéos youtube en mode privé|
|OAndBackupX    |Sauvegarde et restauration applications+données|
|Phonograph     |Lecture musique|
|Open Camera    |Application photo|
|Langis ou Signal|Application SMS|
|VPN Hotspot    |Permet de partager son VPN au travers un partage de connexion|
|Ameixa icons   |Pack d'îcones|
|AntennaPod     |Application pour podcasts|
|Anuto TD       |Jeu de Tower Defense|
|VLC            |Lecteur Vidéo|
|orbot          |Nécéssaire pour utiliser orfox|
|orfox          |Navigateur avec TOR intégré|
|bitwarden      |Client pour le gestionnaire de mots de passes Bitwarden|
|checkers       |Jeu d'echecs|
|cpu info       |Pour avoir des infos sur le CPU et le matériel|
|crosswords     |Jeu de scrabble|
|dandelion*     |Client pour diaspora|
|DAVx5          |Pour synchroniser son calendrier|
|Firefox Klar   |Firefox en mode incognito|
|Feel           |Application pour le sport|
|Fennec F-droid |Navigateur Web|
|Frozen Bubble  |Jeu|
|Gadgetbridge   |Application pour connecter une montre connectée|
|ICSx5          |Pour rajouter un calendrier au format ICS|
|KDEconnect     |Pour connecter le téléphone à un PC sous linux|
|LeMonde.fr     |Pour avoir les grands titres du Monde.fr|
|Libretrivia    |Jeu de trivial pursuit|
|Fedilab        |Application pour Mastodon/Peertube|
|Librera Reader |Visionneuse de documents PDF, ebooks...|
|Mysplash       |Client pour unsplash|
|Nextcloud      |Client NextcloudS|
|Notes          |Prise de notes via serveur Nextcloud|
|Open Note Scanner  |Pour scanner des documents|
|Openl          |Application de traduction via Deepl|
|openScale      |Application prise de poids|
|OSMAnd~        |Navigation GPS|
|Peertube       |Client peertube|
|RadioDroid     |Application pour streaming radio|
|Infinity      |Client pour reddit|
|Riot.im        |Client pour riot.im|
|SecScanQR      |Pour scanner les QR codes|
|Open Sudoku    |Jeu de sudoku|
|SuperTuxKart   |Jeu de course|
|Telegram FOSS  |Telegram|
|Flym           |Suivre flux d'actualités|
|Transportr     |Application pour les transports en commun|
|Turo           |Jeu|
|ProtonVPN      |Application pour ProtonVPN|
|Unit Converter Ultimate     |Pour la convrsion d'unités|
|Geometric Weather|Prévisions Météo|
|WIFIAnalyzer   |Analyse les résaux wifi aux alentours|
|Hypatia        |Scanner les malwares|
|GMaps WV       |Wrapper de la version web de Google Maps (si OSM ne suffit pas)|
|LibreOffice Viewer|Pour lire des fichiers odt, doc....|
|Suntimes       |Pour connaître heure du lever/coucher du soleil|
|SurvivalManual |Guide de survie|
|Shelter        |Isoler une app (profil pro)|
|Traffic Transport Parisiens|Pour savoir l'état du traffic des transport en IdF|
|FluffyChat     |Client chat riot/matrix|
|Shade Launcher |Launcher personnalisable|
|Yet Another Call Blocker|Bloqueur d'appels spam|
|Covid19 Stats  |Suivre les stats de l'épidémie|
|Exodus Privacy |Vérifier le respect de la vie privée des apps installées|
|UntrackMe      |Redirection Youtube vers Invadious, Twitter vers Nitter|

  * Aurora Store (pour apps du Google Play) disponible [ici](https://www.auroraoss.com/), prenez les Aurora Services pour automatiser les installations de paquets.

## Blocage Pubs

Il faut installer [Adaway](https://f-droid.org/packages/org.adaway/). Il faut rooter le téléphone pour l'utiliser, pour ca il faut soit installer [le zip de LineageOS](https://lineageos.mirrorhub.io/su/) ou l'application [Magisk](https://magiskmanager.com/).

Dans Adaway, on retire tout les URLs de fichier et on ajoute celui de SebSauvage:

    https://sebsauvage.net/hosts/hosts
    
## DNS

Vous pouvez utiliser le DNS de votre VPN ou aller dans les paramètres pour appliquer un DNS particulier, vous en avez plein dans cette [liste](https://en.wikipedia.org/wiki/Public_recursive_name_server).

## Captive Portals

On remplace l'url de google pour le captive portal (HTTP 204) par une autre:

    su
    su
    settings put global captive_portal_http_url  http://captiveportal.kuketz.de
    settings put global captive_portal_https_url  https://captiveportal.kuketz.de
    settings put global captive_portal_fallback_url http://captiveportal.kuketz.de
    settings put global captive_portal_other_fallback_urls http://captiveportal.kuketz.de

*http://captiveportal.kuketz.de* est detenu par un allemand chercheur en sécurité informatique mais vous avez aussi *https://e.foundation/net_204/* ou *http://elementary.io/generate_204*


## Plus

Consulter le [canal Telegram Libreware](http://t.me/Libreware) pour plus d'APK utiles et astuces.
