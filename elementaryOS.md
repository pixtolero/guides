# Avoir une elementary OS aux petits oignons !

![Capture](https://framagit.org/pixtolero/guides/raw/master/screenshotelementaryos.jpeg)

## Mise à jour du système

    sudo apt update && sudo apt upgrade
    
## Support pour les PPA

    sudo apt install software-properties-common 

## Elementary Tweaks

    sudo add-apt-repository ppa:philip.scott/elementary-tweaks
    sudo apt install elementary-tweaks

## Eddy (pour installer des .deb)

Installer en cliquant [ici](https://appcenter.elementary.io/com.github.donadigo.eddy)

## Drivers NVIDIA

    sudo add-apt-repository ppa:graphics-drivers/ppa
    sudo apt update
    sudo apt install nvidia-driver-440
    
## Drivers graphiques libres à jour (Mesa, Vulkan...etc)

     sudo add-apt-repository ppa:oibaf/graphics-drivers
     sudo apt-get upgrade

## Logiciels

### Suite Bureautique

    sudo add-apt-repository ppa:libreoffice/ppa
    sudo apt update
    sudo apt install libreoffice libreoffice-l10n-fr libreoffice-style-elementary


### Pour la musique et la radio

Même si Musique est pas mal, lollypop le dépasse et de loin. Il existe un PPA mais la version proposée (pour bionic) est ancienne, préférez donc le flatpak [ici](https://flathub.org/apps/details/org.gnome.Lollypop). Pensez à désinstaller Musique pour libérer de la place.

Pour la radio, il a le flatpak de [shortwave](https://flathub.org/apps/details/de.haeckerfelix.Shortwave).

### Pour écouter les podcasts

Pas mieux que le flatpak de [gPodder](https://flathub.org/apps/details/org.gpodder.gpodder).
   
### Pour lire les vidéos

L'application Vidéos a quelques soucis, celluloid est une très bonne alternative

    sudo add-apt-repository ppa:xuzhen666/gnome-mpv
    sudo add-apt-repository ppa:mc3man/bionic-media
    sudo apt install celluloid 

Celluloid est une interface pour mpv, voici [une liste de scripts pour mpv](https://github.com/mpv-player/mpv/wiki/User-Scripts).

### Torrents

    sudo apt install com.github.davidmhewitt.torrential
    
Dans les préférences pensez à rajouter une blocklist depuis ce [site](https://www.iblocklist.com/lists.php)
    
### Pour la Photo

Privilégier le [flatpak de darktable](https://flathub.org/apps/details/org.darktable.Darktable) pour être à jour.

### Gimp

    sudo add-apt-repository -y ppa:otto-kesselgulasch/gimp
    sudo apt update && sudo apt install -y gimp gimp-data gimp-plugin-registry gimp-data-extras

### Pour visionner Youtube en anonyme (Freetube)

Télécharger le .deb [ici](https://github.com/FreeTubeApp/FreeTube/releases)

### Dernière version de youtube-dl

    sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
    sudo chmod a+rx /usr/local/bin/youtube-dl

### Jeux

    sudo add-apt-repository ppa:samoilov-lex/gamemode
    sudo apt update
    sudo apt install -y steam gamemode
    sudo add-apt-repository ppa:lutris-team/lutris
    sudo apt install lutris
    sudo add-apt-repository ppa:tkashkin/gamehub
    sudo apt update
    sudo apt install com.github.tkashkin.gamehub
    cd /tmp
    wget -c http://nuts.itch.zone/download/linux
    chmod +x itch-setup
    ./itch-setup
    
* Pensez à activer Proton (steamplay) dans steam
* Pour activer le gamemode à un jeu, dans les options de lancement ajoutez: gamemoderun %command%

    
### Gestion de mots de passe

  * Bitwarden, flatpak [ici](https://flathub.org/apps/details/com.bitwarden.desktop)
  
### Regarder la télévision

  * Molotov, appimage [ici](https://www.molotov.tv/download)
  
### Messageries

  * Signal Desktop, flatpak [ici](https://flathub.org/apps/details/org.signal.Signal)
  * Telegram Desktop, flatpak [ici](https://flathub.org/apps/details/org.telegram.desktop)
  
### Clouds

  * Cozy, appimage [ici](https://github.com/cozy-labs/cozy-desktop/releases)
  * Megasync, paquet deb [ici](https://mega.nz/linux/MEGAsync/xUbuntu_16.04/amd64/megasync-xUbuntu_16.04_amd64.deb)

#### Nextcloud

    sudo add-apt-repository ppa:nextcloud-devs/client
    sudo apt update
    sudo apt install nextcloud-client
    
### Réseau TOR (navigateur et partage de fichiers)

    sudo add-apt-repository ppa:micahflee/ppa
    sudo apt install -y onionsharetorbrowser-launcher
    
### Flatpaks

Allez faire un tour sur [Flathub](https://flathub.org/), liquer sur le bounton install Sideload et AppCenter s'occupe du reste :)

Pour nettoyer les flatpaks inutiles:

    flatpak uninstall --unused
    
[Guide](https://itsfoss.com/flatpak-guide/)

### Virtualbox

    sudo nano /etc/apt/sources.list.d/virtualbox.list

Et on ajoute:

    deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian bionic contrib

On ajoutre la clée publique

    wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -

On met ensuite les dépôts à jour et on installe virtualbox (remplacer 6.1 par la version à installer) :

    sudo apt-get update
    sudo apt-get install virtualbox-6.1
    
### Hacking

Outils de hacking avec fsociety3

    cd /tmp
    git clone https://github.com/CRO-THEHACKER/fsociety3.git
    cd fsociety3
    chmod +x INSTALL
    ./INSTALL
    
### protonVPN

Si vous avez un compte (gratuit ou pas) chez ProtonVPN,

    sudo apt install -y openvpn dialog python3-pip python3-setuptools

On installe protonvpn-cli

    sudo pip3 install protonvpn-cli

Lancer la config avec:

    sudo protonvpn init

Pour l'application graphique:

    sudo apt install -y python3-gi python3-gi-cairo gir1.2-gtk-3.0 gir1.2-appindicator3 libnotify-bin libpolkit-agent-1-0
    sudo pip3 install protonvpn-gui

Créer un fichier *protonvpn-gui.desktop* dans *~/.local/share/applications/* avec:

`[Desktop Entry]`
`Name=ProtonVPN GUI`
`GenericName=Unofficial ProtonVPN GUI for Linux`
`Exec=sudo protonvpn-gui`
`Icon=/usr/local/lib/python3.6/dist-packages/protonvpn_linux_gui/resources/protonvpn_logo.png`
`Type=Application`
`Terminal=False`
`Categories=Utility;GUI;Network;VPN`

Donnez l'accès à l'utilisateur:

    sudo visudo

et ajouter (remplacer login par votre nom d'utilisateur):

    login ALL = (root) NOPASSWD:/usr/local/bin/protonvpn-gui
    
 Dans paramètres --> Applications au démarrage rajouter comme commande personalisée *protonvpn-tray* afin d'avoir une icone en zone de notification au démarrage.
 
 ### Bpytop
 
 Bashtop en python, pour monitorer depuis le terminal, pour l'installer:
 
    pip3 install bpytop --upgrade

## Autonomie pour les laptops

    sudo add-apt-repository ppa:linrunner/tlp
    sudo apt update
    sudo apt-get install tlp tlp-rdw
    sudo tlp start
    
## Sons de relaxation

    git clone https://github.com/nick92/tranqil.git
    cd tranqil/
    sudo apt install libgtk-3-dev libgstreamer1.0-dev valac meson
    meson build --prefix=/usr
    cd build/
    ninja
    sudo ninja install

## Apparence

### Fonds d'écran

Fondo permet d'installer des fonds pris depuis Unsplash

    sudo apt-get install com.github.calo001.fondo
    
### Wingpanel-indicator-ayatana

Permet d'avoir les indicateurs non-visibles nativement

    sudo apt-get install libglib2.0-dev libgranite-dev libindicator3-dev
    git clone https://github.com/Lafydev/wingpanel-indicator-ayatana.git
    cd wingpanel-indicator-ayatana
    meson build --prefix=/usr
    cd build
    ninja
    sudo ninja install
    sudo apt-get install libwingpanel-2.0-dev valac gcc meson /OnlyShowIn=Unity;GNOME;Pantheon;/g' /etc/xdg/autostart/indicator-application.desktop
    sudo rm -f /etc/xdg/autostart/nm-applet.desktop  
    
### Wingpanel-monitor

    sudo apt install libglib2.0-dev libgtop2-dev libgranite-dev libgtk-3-dev libwingpanel-2.0-dev libgeoclue-2-dev libgweather-3-dev meson valac
    cd Projets (prenez l'emplacement que vous voulez)
    git clone https://github.com/PlugaruT/wingpanel-monitor.git
    cd wingpanel-monitor
    meson build --prefix=/usr
    cd build
    ninja
    sudo ninja install
    
   Lancez la commande *com.github.plugarut.wingpanel-monitor*
   
### Icônes de la zone de notification

Pour les avoir toutes en monochrome et uniforme au thème par défaut, on va compiler et utiliser hardcode-tray

    sudo apt install git build-essential meson libgirepository1.0-dev libgtk-3-dev python3 python3-gi gir1.2-rsvg-2.0 librsvg2-bin gir1.2-gtk-3.0
    cd /tmp
    git clone https://github.com/bil-elmoussaoui/Hardcode-Tray
    cd Hardcode-Tray
    meson builddir --prefix=/usr
    sudo ninja -C builddir install
   
Et enfin:

    sudo -E hardcode-tray --conversion-tool RSVGConvert --size 24 --theme elementary

### Thème dark pour Plank

    cd /tmp
    git clone https://github.com/korbsstudio/eOS-Dark-Plank-Theme
    cd eOS-Dark-Plank-Theme
    ./install.sh
    
   Dans les préférences de Plank, prenez eOS-dark comme thème.
   
   Il existe d'autres thèmes pour plank [ici](https://github.com/kennyh0727/plank-themes).

### Ulauncher (lanceur d'apps)

    sudo add-apt-repository ppa:agornostal/ulauncher
    sudo apt update
    sudo apt-get install ulauncher
    
Téléchargez le thème [ici](https://github.com/sandnap/ulauncher-elementary/raw/master/ulauncher-elementary.zip) et placez le contenu dans ~/.config/ulauncher

### Theme elementary pour Firefox

    curl -s -o- https://raw.githubusercontent.com/Zonnev/elementaryos-firefox-theme/master/install.sh | bash

Dans about:config activez browser.urlbar.megabar
Dans personalisation désactivez la barre de titre

### Thème pour Telegram

C'est [ici](https://github.com/sprite-1/elementary-patches/tree/master/apps/elementaryos_theme_for_telegram)


### Zsh tout beau dans le terminal

### Installation de zsh, oh-my-zsh et thème Powerline9k

    sudo apt-get install zsh git wget powerline fonts-powerline -y
    wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O - | sh
    git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k
    
Puis mettre **ZSH_THEME="powerlevel9k/powerlevel9k"** à ~/.zshrc
    
### plugins pour oh-my-zsh

    git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

### LSD

    cd /tmp
    wget https://github.com/Peltoche/lsd/releases/download/$LSD_VERSION/lsd_0.16.0_amd64.deb
    sudo dpkg -i lsd_0.16.0_amd64.deb
    
Puis ajouter à ~/.bashrc ou ~/.zshrc

    alias ls='lsd'

### Police Hack   

Téléchargez la police [ici](https://github.com/source-foundry/Hack/releases/download/v3.003/Hack-v3.003-ttf.zip).
Puis placer dans *~/.local/share/fonts/*. On regnère le cache avec:

    fc-cache -f -v

Dans Tweaks, onglet Polices, à Monospace font mettez *Hack Regular*

### Web Apps

Il est possible de faire des applications web pour Netflix, Prime Vidéo, plus d'explications [ici](https://framagit.org/pixtolero/elementaryos-webapps).

### Retirer l'icone de Plank

Pour retirer l'icone de Plank, avec dconf-editor allez dans *net/launchpad/plank/docks/dock1* et décocher l'option *show-dock-item*.

### Fond d'écran dynamique

[Projet](https://github.com/adi1090x/dynamic-wallpaper) qui fonctionne très bien avec Pantheon.
