# Guide Post-Installation Pop OS! 19.04

![Capture](https://framagit.org/pixtolero/guides/raw/master/screenshotpopos1904.png)

## Mise à jour du système

    sudo apt update && sudo apt upgrade

## GNOME

### Logiciel _Ajustements_

    sudo apt install gnome-tweak-tool

  * Retour du clic droit, dans la partie Clavier et Souris, cocher "Surface" en bas
  * Pour les portable, il y a l'option pour afficher le pourcentage de la batterie

### Extensions gnome-shell indispensables

    sudo apt install chrome-gnome-shell

  * [Liste des extensions indispensables](https://framagit.org/pixtolero/guides/blob/master/extensions-gnome-shell.md)

### Raccourcis GNOME à savoir

#### Window & Workspace Management

| Raccourci | Action  | Avec avec <kbd>Shift</kbd> |
| -------- | --------- | ------ |
| <kbd>Super</kbd> + <kbd>Tab</kbd> | Changer de fenêtre | Changer de fenêtre dans le sens inverse |
| <kbd>Super</kbd> + Above <kbd>Tab</kbd> | Changer de fenêtre de l'application en cours | Changer de fenêtre de l'application en cours dans le sens inverse |
| <kbd>Super</kbd> + <kbd>W</kbd> | Fermer la fenêtre | |
| <kbd>Super</kbd> + <kbd>H</kbd> | Masquer la fenêtre (minimiser) | |
| <kbd>Super</kbd> + <kbd>↑</kbd>/<kbd>↓</kbd> | Changer d'espace de travail haut/bas | Changer d'espace de travail haut/bas avec la fenêtre |
| <kbd>Super</kbd> + <kbd>Home</kbd>/<kbd>End</kbd> | Changer pour le prémier/dernier espace de travail | Changer pour le prémier/dernier espace de travail  avec la fenêtre |
| <kbd>Ctrl</kbd> + <kbd>Super</kbd> + <kbd>←</kbd>/<kbd>→</kbd> | Placer (tiling) la fenêtre sur la gauche/droite | |
| <kbd>Ctrl</kbd> + <kbd>Super</kbd> + <kbd>↑</kbd> | Maximiser | |
| <kbd>Ctrl</kbd> + <kbd>Super</kbd> + <kbd>↓</kbd> | Restaurer la fenêtre (dé-maximiser/untile) | |
| <kbd>Shift</kbd> + <kbd>Ctrl</kbd> + <kbd>Super</kbd> + <kbd>←</kbd>/<kbd>→</kbd>/<kbd>↑</kbd>/<kbd>↓</kbd> | Déplacer la fenêtre à afficher sur la gauche, droite, en haut ou en bas | |
| <kbd>Super</kbd> + Drag | Move window | |
| <kbd>Super</kbd> + Right-drag | Changer la taille de la fenêtre | |
| <kbd>Alt</kbd> + <kbd>Space</kbd> | Menu de la fenêtre |

#### Operating System

| Raccourci | Action |
| -------- | ------ |
| <kbd>Super</kbd> | Aperçu Activités |
| <kbd>Super</kbd> + <kbd>A</kbd> | Applications |
| <kbd>Super</kbd> + <kbd>P</kbd> | Mode de présentation (cycle) |
| <kbd>Super</kbd> + <kbd>L</kbd> | Vérouiller l'écran |
| <kbd>Super</kbd> + <kbd>O</kbd> | Vérouiller l'orientation (si accéléromètre) |
| <kbd>Super</kbd> + <kbd>D</kbd> | Montrer le bureau |
| <kbd>Super</kbd> + <kbd>V</kbd> | Calendrier et Notifications |
| <kbd>Super</kbd> + <kbd>N</kbd> | Focus sur la nottification active |
| <kbd>Super</kbd> + <kbd>T</kbd> | Terminal |
| <kbd>Super</kbd> + <kbd>M</kbd> | Menu Application |
| <kbd>Super</kbd> + <kbd>Space</kbd> | Changer de source d'entrée |
| <kbd>Alt</kbd> + <kbd>F2</kbd> | Pour lancer une commande |
| <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>Del</kbd> | Se déconnecter de la session |


## Logiciels à installer

### Codecs

    sudo apt install ubuntu-restricted-extras

### Suite bureautique

    sudo add-apt-repository ppa:libreoffice/ppa
    sudo apt update
    sudo apt install libreoffice libreoffice-l10n-fr

### Pour la Photo

    sudo add-apt-repository ppa:otto-kesselgulasch/gimp
    sudo apt update
    sudo apt install gimp gmic
    sudo apt install darktable shotwell rapid-photo-downloader

#### darktable

Récupérer le paquet deb [ici](https://software.opensuse.org/download.html?project=graphics:darktable:stable&package=darktable) ou la version dev [ici](https://download.opensuse.org/repositories/graphics:/darktable:/master/)

### Pour la musique

    sudo add-apt-repository ppa:gnumdk/lollypop
    sudo apt update
    sudo apt install lollypop

### Pour écouter les stations radio

    sudo add-apt-repository ppa:shortwave-developers/shortwave-nightly
    sudo apt update
    sudo apt install shortwave


### Internet

    sudo apt install transmission filezilla youtube-dl uget grsync liferea geary

#### protonvpn-cli

    sudo apt install resolvconf
    sudo wget -O protonvpn-cli.sh https://raw.githubusercontent.com/ProtonVPN/protonvpn-cli/master/protonvpn-cli.sh
    sudo chmod +x protonvpn-cli.sh
    sudo ./protonvpn-cli.sh --install
    sudo protonvpn-cli -init

### Applications web

  * Bitwarden, paquet deb [ici](https://github.com/bitwarden/desktop/releases)
  * Mattermost-desktop, paquet deb [ici](https://github.com/mattermost/desktop/releases)
  * Protonmail-bridge, paquet deb [ici](https://protonmail.com/download/protonmail-bridge_1.1.6-1_amd64.deb)
  * DuckieTV, paquet deb [ici](https://github.com/DuckieTV/Nightlies/releases)
  * Molotov, appimage [ici](https://www.molotov.tv/download)

#### Telegram

    sudo apt install telegram-desktop

### Qarte

    sudo add-apt-repository ppa:vincent-vandevyvre/vvv
    sudo apt update
    sudo apt install qarte

### Clouds

  * Cozy, appimage [ici](https://github.com/cozy-labs/cozy-desktop/releases)
  * Megasync, paquet deb [ici](https://mega.nz/linux/MEGAsync/xUbuntu_19.04/amd64/megasync-xUbuntu_19.04_amd64.deb)
  * Nautilus-Megasync, paquet deb [ici](https://mega.nz/linux/MEGAsync/xUbuntu_19.04/amd64/nautilus-megasync-xUbuntu_19.04_amd64.deb)

#### Nextcloud

    sudo add-apt-repository ppa:nextcloud-devs/client
    sudo apt update
    sudo apt install nextcloud-client

#### Dropbox

    sudo apt install nautilus-dropbox

### Jeux

    sudo apt install steam

#### Gamehub et Lutris

    sudo apt install com.github.tkashkin.gamehub lutris

### Divers

    sudo apt install calibre baobab testdisk eog eog-plugins gnome-calendar gufw deja-dup caffeine cheese vokoscreen neofetch gnome-maps gnome-weather gnome-calculator gnome-clocks simple-scan gpodder gnome-documents gnome-contacts virtualbox virtualbox-ext-pack bleachbit memtest86+ passbook popsicle

  * multibootusb, paquet deb [ici](http://multibootusb.org/page_download/)
  * qomui, paquet deb [ici](https://github.com/corrad1nho/qomui/releases)

### TOR

    sudo apt install torbrowser-launcher onioncircuits

### onionshare (partage de fichiers)

    sudo add-apt-repository ppa:micahflee/ppa
    sudo apt install -y onionshare

### Virtualbox 6

Prérequis:

    sudo apt update
    sudo apt-get install gcc make linux-headers-$(uname -r) dkms

Rajout du dépôt:

    wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
    wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
    sudo sh -c 'echo "deb http://download.virtualbox.org/virtualbox/debian $(lsb_release -sc) contrib" >> /etc/apt/sources.list.d/virtualbox.list'

On retire l'ancienne version:

    sudo apt remove virtualbox virtualbox-5.2

Installation:

    sudo apt update
    sudo apt-get install virtualbox-6.0


#### Flatpaks

Rajout du dépôt flathub

    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

Freetube

    flatpak install flathub io.freetubeapp.FreeTube

Podcasts

    flatpak install flathub org.gnome.Podcasts

Uberwriter

    flatpak install flathub de.wolfvollprecht.UberWriter

Pour lister les paquets flatpaks

    flatpak list

Pour les mettre à jour

    latpak update nom.du.flatpak


## Optimisation

### Preload

    sudo apt install preload

### Nettoyage cache paquets

    sudo apt clean
    sudo apt autoremove

### Oh my ZSH

    sudo apt install git-core zsh
    cd /tmp
    sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
    sudo apt install fonts-powerline zsh-syntax-highlighting

Plugins en plus

    cd ~/.oh-my-zsh/custom/plugins
    git clone https://github.com/zsh-users/zsh-autosuggestions

Éditer votre config (plugins, thème):

    nano ~/.zshrc

[Liste des thèmes](https://github.com/robbyrussell/oh-my-zsh/wiki/Themes)
[Liste des plugins](https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins)

Thème Powerline9K

    sudo apt install zsh-theme-powerlevel9k

Puis mettre **ZSH_THEME="powerlevel9k/powerlevel9k"** à ~/.zshrc


Mettre zsh par défaut:

    chsh -s $(which zsh)

[Site](ohmyz.sh)

### LSD (meilleure que la commande ls)

Installer le paquet deb se trouvant [ici](https://github.com/Peltoche/lsd/releases)

Puis ajouter à ~/.bashrc ou ~/.zshrc

    alias ls='lsd'

Polices utiles pour les icônes:

    sudo apt install fonts-powerline fonts-font-awesome

et nerd fonts:

    cd /tmp
    git clone https://github.com/ryanoasis/nerd-fonts.git
    cd nerd-fonts
    ./install.sh

## A savoir

### Virtualbox

Soucis avec modprobe vboxdrv ?

    sudo apt update
    sudo apt install --reinstall linux-headers-$(uname -r) virtualbox-dkms dkms
    sudo modprobe vboxdrv

### Retirer anciens kernels

    dpkg -l | tail -n +6 | grep -E 'linux-image-[0-9]+' | grep -Fv $(uname -r)

Cette commande liste les kernels disponibles avec les mentions suivantes:

  * *rc*: déjà retiré
  * *ii*: installé mais peut être retiré
  * *iU*: A ne pas retirer, prévu dans les mises à jour

Ensuite retirer le kernel avec *sudo apt remove*


### Imprimantes HP

    sudo apt install hplip

 * Pour les modifs:

    sudo usermod -aG lpadmin username


### Menu de boot

 * Garder la barre Espace enfoncée pour avoir le menu au boot

### Activer OpenVPN

    sudo apt install network-manager-openvpn-gnome

### UKUU

   * paquet deb [ici](https://github.com/teejee2008/ukuu/releases/download/v18.9.1/ukuu-v18.9.1-amd64.deb)
