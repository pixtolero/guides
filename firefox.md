# Paramétrages pour Firefox

## Extensions indispensables

  * [Ublock Origin](https://addons.mozilla.org/fr/firefox/addon/ublock-origin/?src=search)
  * [Nano Defender](https://addons.mozilla.org/fr/firefox/addon/nano-defender-firefox/?src=search)
  * [Kimetrak](https://addons.mozilla.org/fr/firefox/addon/kimetrak/)
  * [Decentraleyes](https://addons.mozilla.org/fr/firefox/addon/decentraleyes/)
  * [Disconnect](https://addons.mozilla.org/fr/firefox/addon/disconnect/?src=search)
  * [Facebook Container](https://addons.mozilla.org/fr/firefox/addon/facebook-container/?src=search)
  * [Grammalecte](https://addons.mozilla.org/fr/firefox/addon/grammalecte-fr/?src=search)
  * [GSconnect](https://addons.mozilla.org/fr/firefox/addon/gsconnect/?src=search)
  * [Privacy Badger](https://addons.mozilla.org/fr/firefox/addon/privacy-badger17/?src=search)
  * [Privacy Possum](https://addons.mozilla.org/fr/firefox/addon/privacy-possum/?src=search)
  * [Smart Referer](https://addons.mozilla.org/fr/firefox/addon/smart-referer/?src=search)
  * [TrackMeNot](https://addons.mozilla.org/fr/firefox/addon/trackmenot/?src=search)
  * [Terms of Service; Didn’t Read](https://addons.mozilla.org/fr/firefox/addon/terms-of-service-didnt-read/)
  * [Bitwarden](https://addons.mozilla.org/fr/firefox/addon/bitwarden-password-manager/?src=search)
  * [Awesaome RSS](https://addons.mozilla.org/fr/firefox/addon/awesome-rss/?src=search)
  * [Buster: Captcha Solver for Humans](https://addons.mozilla.org/fr/firefox/addon/buster-captcha-solver/?src=search)
  * [CanvasBlocker](https://addons.mozilla.org/fr/firefox/addon/canvasblocker/?src=search)
  * [ClearURLs](https://addons.mozilla.org/fr/firefox/addon/clearurls/)
  * [Dark Reader](https://addons.mozilla.org/fr/firefox/addon/darkreader/?src=search)
  * [Flagfox](https://addons.mozilla.org/fr/firefox/addon/flagfox/?src=search)
  * [Search by Image](https://addons.mozilla.org/fr/firefox/addon/search_by_image/?src=search)
  * [Invidioucious](https://addons.mozilla.org/fr/firefox/addon/invidioucious/?src=search)
  * [IP Address and Domain Information](https://addons.mozilla.org/fr/firefox/addon/ip-address-and-domain-info/?src=search)
  * [Librefox HTTP Watcher - Red flag](https://addons.mozilla.org/fr/firefox/addon/librefox-http-watcher-red-flag/?src=search)
  * [Librefox Reload/Refresh Button Url Bar](https://addons.mozilla.org/fr/firefox/addon/librefox-reload-button-url-bar/?src=search)
  * [Location Guard](https://addons.mozilla.org/fr/firefox/addon/location-guard/?src=search)
  * [Mastodon – Simplified Federation!](https://addons.mozilla.org/fr/firefox/addon/mastodon-simplified-federation/?src=search)
  * [QookieFix](https://addons.mozilla.org/fr/firefox/addon/qookiefix/?src=search)
  * [Share-freedom](https://addons.mozilla.org/fr/firefox/addon/share-freedom/?src=search)
  * [Temp Mail - E-mail temporaire disponible](https://addons.mozilla.org/fr/firefox/addon/temp-mail/?src=search)
  * [Youtube Container](https://addons.mozilla.org/fr/firefox/addon/youtube-container/?src=search)

## Paramétrage d'Ublock Origins

### Paramètre à activer

  * Empêcher la fuite des adresses IP locales via WebRTC
  * Bloquer les rapports CSP

### Listes à rajouter

  * [Fanboy's Cookiemonster](abp:subscribe?location=https%3A%2F%2Ffanboy.co.nz%2Ffanboy-cookiemonster.txt&amp;title=Fanboy's%20Cookiemonster%20List)
  * [CrapBlock Annoyances](abp:subscribe?location=https%3A%2F%2Fraw.githubusercontent.com%2Ftheel0ja%2FCrapBlock%2Fmaster%2Fcrapblock-annoyances.txt&amp;title=CrapBlock%20Annoyances)
  * [uBlock Filters Plus](abp:subscribe?location=https%3A%2F%2Fraw.githubusercontent.com%2FIDKwhattoputhere%2FuBlock-Filters-Plus%2Fmaster%2FuBlock-Filters-Plus.txt&amp;title=uBlock%20Filters%20Plus)
  * [AdGuard Annoyances Filter](abp:subscribe?location=https%3A%2F%2Ffilters.adtidy.org%2Fextension%2Fchromium%2Ffilters%2F14.txt&amp;title=AdGuard%20Annoyances%20Filter)
  * [Fanboy Annoyances List%20(Domains)](abp:subscribe?location=https%3A%2F%2Fraw.githubusercontent.com%2Fdeathbybandaid%2Fpiholeparser%2Fmaster%2FSubscribable-Lists%2FParsedBlacklists%2FFanboy-Annoyances-List.txt&amp;title=Fanboy%20Annoyances%20List%20(Domains))
  * [Adblock Warning Removal List](abp:subscribe?location=https%3A%2F%2Feasylist-downloads.adblockplus.org%2Fantiadblockfilters.txt&amp;title=Adblock%20Warning%20Removal%20List)


Plus de filtres sur [Filterlists](https://filterlists.com/)

## Paramétrage de Nano Defender

On active [Adblock Warning Removal List](ubo://subscribe?location=https%3A%2F%2Feasylist-downloads.adblockplus.org%2Fantiadblockfilters.txt&title=Adblock%20Warning%20Removal%20List)

On souscris au filtre [Nano Defender Integration](ubo://subscribe?location=https%3A%2F%2Fgitcdn.xyz%2Frepo%2FNanoAdblocker%2FNanoFilters%2Fmaster%2FNanoMirror%2FNanoDefender.txt&title=Nano%20Defender%20Integration)

Dans l'onglet Paramètres, cliquez sur la roue crantée des fonctionalités avancées puis à **userResourcesLocation** mettre la valeur (à la place de unset) **https://gitcdn.xyz/repo/NanoAdblocker/NanoFilters/master/NanoFilters/NanoResources.txt**

On souscris aux filtres [Nano filters](ubo://subscribe?location=https%3A%2F%2Fgitcdn.xyz%2Frepo%2FNanoAdblocker%2FNanoFilters%2Fmaster%2FNanoFilters%2FNanoBase.txt&title=Nano%20filters)

On souscris aux filtres [Nano filters - Whitelist)[ubo://subscribe?location=https%3A%2F%2Fgitcdn.xyz%2Frepo%2FNanoAdblocker%2FNanoFilters%2Fmaster%2FNanoFilters%2FNanoWhitelist.txt&title=Nano%20filters%20-%20Whitelist)

[Source](https://jspenguin2017.github.io/uBlockProtector/)


## Paramètres spécifiques

Dans _about:config_

  * `browser.ctrlTab.recentlyUsedOrder = false`
  * `view_source.wrap_long_lines = true`
  * `extensions.pocket.enabled = false`
  * `privacy.resistFingerprinting = true`
  * `privacy.trackingprotection.fingerprinting.enabled = true`
  * `privacy.trackingprotection.cryptomining.enabled = true`
  * `privacy.trackingprotection.enabled = true`
  * `geo.enabled = false`
  * `network.prefetch-next = false`
  * `network.dns.disablePrefetch`
  * `browser.cache.memory.max_entry_size=50000`
  * `browser.cache.disk.enable = false`


Dans _about:preferences_
  * Dans Vie privée et sécurité: Décocher Autoriser Firefox à envoyer des données techniques et des données d’interaction à Mozilla. Décocher Autoriser Firefox à envoyer pour vous les rapports de plantage en attente
  * Dans les paramètres Réseau, activer **Activer le DNS via HTTPS** et prendre un serveur personalisé comme **https://doh.securedns.eu/dns-query** (liste [ici](https://github.com/curl/curl/wiki/DNS-over-HTTPS#publicly-available-servers))
